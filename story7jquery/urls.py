from django.urls import path
from .views import story7Views

urlpatterns = [
    path('', story7Views, name='story7Views'),
]
