from django.test import TestCase, Client
from django.urls import resolve
from .views import story7Views


class Story7UnitTest(TestCase):

    def test_story7_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_story7_using_story7Views_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7Views)

    def test_story7_using_story7_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')

    # def test_page_contains_accordion_content(self):
    #     response = Client().get('/story7/')
    #     page = response.content.decode("utf8")
    #     self.assertIn("")
    