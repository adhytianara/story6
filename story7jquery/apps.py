from django.apps import AppConfig


class Story7JqueryConfig(AppConfig):
    name = 'story7jquery'
