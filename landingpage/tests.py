from django.test import TestCase, Client, LiveServerTestCase
from django.utils import timezone
from django.urls import resolve
from .views import landingpageViews
from .models import landingpageModels 

from selenium import webdriver
import time
import os
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story6.settings import BASE_DIR


class LandingPageUnitTest(TestCase):

    def test_landingpage_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_view(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpageViews)

    def test_landingpage_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_landingpage_models_can_create_new_object(self):
        landingpageModels.objects.create(status='cuaca hari ini cerah')
        count = landingpageModels.objects.all().count()
        self.assertEqual(count, 1)

    def test_form(self):
        response = Client().get('')
        self.assertContains(response, '<form')

    def test_post_forms(self):
        data = {
            'status': 'cuaca hari ini cerah',
            'waktu': timezone.now
        }
        response = Client().post('', data)
        self.assertEqual(response.status_code, 200)

    def test_default_forms(self):
        response = Client().post('')
        self.assertEqual(response.status_code, 200)


class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR, "chromedriver"), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self): 
        self.browser.get(self.live_server_url+"/")
        time.sleep(2)  # Let the user actually see something!
        self.assertIn('Halo, bagaimana kabarmu?', self.browser.title)

        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Halo, apa kabar?', header_text)

        # find the form element
        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('id_submit')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(3)  # Let the user actually see something!

        self.assertIn('Coba Coba', self.browser.page_source)
