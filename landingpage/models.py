from django.db import models
from django.utils import timezone


class landingpageModels(models.Model):
    status = models.CharField(max_length=500)
    waktu = models.DateTimeField(default=timezone.now)
