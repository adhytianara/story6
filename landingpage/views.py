from django.shortcuts import render
from .forms import landingpageForms
from .models import landingpageModels
from django.utils import timezone

def landingpageViews(request):
    form = landingpageForms(request.POST or None)
    data = landingpageModels.objects.all()
    dictio = {}
    dictio['form'] = form
    dictio['data'] = data
    if request.method == "POST":
        if form.is_valid():
            model = landingpageModels(status=form['status'].value())
            model.save()
            return render(request, 'landingpage.html', dictio)
    elif data.count != 0:
        return render(request, 'landingpage.html', dictio)
    return render(request, 'landingpage.html', dictio)
