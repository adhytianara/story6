from django.urls import path
from .views import landingpageViews

urlpatterns = [
    path('', landingpageViews, name='landingpage')
]
