from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

import json
import requests


def story8Views(request):
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + "django")
    return render(request, "story8.html", {"result": r.json()})


def listBooks(request):
    q = request.GET.get('q', 'zombie')
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    return JsonResponse(r.json())
