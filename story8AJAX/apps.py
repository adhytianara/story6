from django.apps import AppConfig


class Story8AjaxConfig(AppConfig):
    name = 'story8AJAX'
