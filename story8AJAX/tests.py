from django.test import TestCase, Client
from django.urls import resolve
from .views import story8Views, listBooks
import json


class Story8UnitTest(TestCase):

    def test_story8_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_story8Views_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8Views)

    def test_story8_using_story8_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_url_AJAX(self):
        response = self.client.get("/story8/listbooks/")
        self.assertEqual(response.status_code, 200)

    def test_page_using_AJAX(self):
        response = resolve("/story8/listbooks/")
        self.assertEqual(response.func, listBooks)

    def test_response_JSON_AJAX(self):
        response = self.client.get("/story8/listbooks/?q=kucing")
        self.assertEqual(type(json.loads(response.content)), dict)
