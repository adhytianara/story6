from django.urls import path
from .views import story8Views, listBooks

urlpatterns = [
    path('', story8Views, name='story8Views'),
    path('listbooks/', listBooks, name='listbooks'),
]
