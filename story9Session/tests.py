from django.test import TestCase
from django.urls import resolve
from story9Session.views import signup, home

from django.contrib.auth.models import User

class Story9UnitTest(TestCase):

    def test_url_signup_exist(self):
        response = self.client.get('/story9/signup/')
        self.assertEqual(response.status_code,200)

    def test_url_login_exist(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code,200)

    def test_url_logout_exist(self):
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code,302)

    def test_using_signup_view(self):
        response = resolve('/story9/signup/')
        self.assertEqual(response.func,signup)

    def test_using_home_view(self):
        response = resolve('/story9/')
        self.assertEqual(response.func,home)

    def test_using_login_template(self):
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response,'registration/login.html')

    def test_using_signup_template(self):
        response = self.client.get('/story9/signup/')
        self.assertTemplateUsed(response,'registration/signup.html')

    def test_using_home_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response,'home.html')

    def test_signup(self):
        count = User.objects.all().count()
        self.assertEqual(count, 0)
        
        data = {'username':'putra', 'password1':'5t4r3e2w1q', 'password2':'5t4r3e2w1q'}
        response = self.client.post('/story9/signup/', data)
        count2 = User.objects.all().count()

        self.assertEqual(count2,1)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/accounts/login/',response=response)

    def test_signup_fail(self):
        data = {'username':'putra', 'password1':'5t4r3e2w1q', 'password2':'5t4r3e2w11'}
        response = self.client.post('/story9/signup/', data)
        count = User.objects.all().count()

        self.assertEqual(count, 0)
        self.assertEqual(response.status_code,200)

    def test_session_id_exist_when_logged_in(self):
        user = User.objects.create_user('myusername', 'myemail@crazymail.com', 'mypassword')
        user.first_name = 'Putra'
        user.save()
        response = self.client.post('/accounts/login/', data={'username':'myusername', 'password':'mypassword'})
        self.assertEqual(response.status_code, 302)
        self.assertIn('_auth_user_id', self.client.session)