from django.urls import path
from .views import story10Views

urlpatterns = [
    path('', story10Views, name='story10Views'),
]
