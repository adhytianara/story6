from django.apps import AppConfig


class Story10LoadbalancerConfig(AppConfig):
    name = 'story10LoadBalancer'
